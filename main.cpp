#include "ImageScrambler.h"
#include <QApplication>
#include "qfile.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QFile File(":/style_3");
    File.open(QFile::ReadOnly);
    QString StyleSheet = QLatin1String(File.readAll());

    qApp->setStyleSheet(StyleSheet);
    ImageScrambler w;
    w.setStyleSheet("");
    w.show();

    return a.exec();
}
